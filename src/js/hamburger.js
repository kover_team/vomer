document.getElementById('menu__toggle').onclick = function() {
    if ( this.checked ) {
        document.body.style.overflow = 'hidden';
    } else {
        document.body.style.overflow = 'auto';
    }
};

var bg = document.querySelector('.menu__box_bg');
var m_bg = document.querySelector('.menu__box');

function close() {
    document.getElementById('menu__toggle').checked = false;
    document.body.style.overflow = 'auto';
}

if (bg) {
    bg.addEventListener("click", close);
    m_bg.addEventListener("click", close);
}