const stars = document.querySelectorAll('.star-popup');
let len = stars.length;
let i=0;

stars.forEach((star, i) => {
    star.addEventListener('mouseover', () => {
        clval(i);
    });
    star.addEventListener('mouseout', () => {
        clval(i);
    });
    star.addEventListener('click', () => {
        click(i);
    });
});


const click = function (n) {
    document.querySelector('.num').value = n;
    for (let j=0; j <= n; j++){
        if (stars[n].classList.contains('st1')) {
            for (let j=0; j <= n; n++) {
                stars[n+1].classList.remove('st1');
            }
        } else {
            stars[j].classList.add('st1');
        }
    }
}

function move(e) {
    for (let j=0; j <= len; j++){
        stars[j].classList.add('st2');
    }
}

const clval = function(e) {
    for (let j=0; j <= e; j++){
        if (stars[e].classList.contains('st2')) {
            for (let j=0; j <= len; j++) {
                stars[j].classList.remove('st2');
            }
        } else {
            stars[j].classList.add('st2');
        }
    }
}