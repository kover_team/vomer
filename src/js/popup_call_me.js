var modal = document.querySelector(".modal");
var btns = document.querySelectorAll(".btn-call-me");
var closeButton = document.querySelector(".close-button");

function toggleModal() {
    modal.classList.toggle("show-modal");
    document.body.style.overflow ='hidden';
}

function windowOnClick(event) {
    if (event.target === modal) {
        toggleModal();
        document.body.style.overflow ='auto';
    }
}

function clos(){
    toggleModal();
    document.body.style.overflow ='auto';
}

if (btns[0]){
    btns.forEach(btn => {
        btn.addEventListener("click", toggleModal);
    });
    
    window.addEventListener("click", windowOnClick);
    closeButton.addEventListener("click", clos);
}